<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use GuzzleHttp\Client;

class AppServiceProvider extends ServiceProvider
{
    const API_TIMEOUT = 60;
    const API_TIMEOUT_CONNECT = 60;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $base_bd_api_uri = env('BD_API_BASE_URI');

        $this->app->singleton('bd-client', function() use($base_bd_api_uri) {
            return new Client([
                'base_uri' => $base_bd_api_uri,
                'verify' => false,
                'timeout' => self::API_TIMEOUT,
                'timeout_connect' => self::API_TIMEOUT_CONNECT,
            ]);
        });

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
