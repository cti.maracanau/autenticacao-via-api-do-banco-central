<?php

namespace App\Helpers\Enumerations;

use MyCLabs\Enum\Enum;

/**
 * @method static self QUERY()
 * @method static self SEARCH()
 * @method static self AUTH()
 * @method static self REGISTER()

 */

class BDParams extends Enum {

    public const QUERY = 'data';
    public const SEARCH = 'data/id';
    public const AUTH = 'login';
    public const REGISTER = 'register';

}

?>
