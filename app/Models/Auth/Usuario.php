<?php

namespace App\Models\Auth;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable {

    use Notifiable;

    protected $table = 'users';

    protected $primaryKey = "pessoa_id";
    protected $rememberToken = "remember_token";

    protected $fillable = [
        'pessoa_id', 'servidor_id', 'identificacao', 'nome', 'email', 'password', "nascimento",
        "nomeCurso", "tipoPessoa", "nomeCategoria", "nomeSetor", "nomeLocal", "descricao",
        "token", "remember_token", "data_de_criacao", "ultima_atualizacao",
    ];

    // const CREATED_AT = "data_de_criacao";
    // const UPDATED_AT = "ultima_atualizacao";

    protected $dates = [
        '
        ', 'data_criacao', 'data_atualizacao',
    ];

    protected $hidden = [
        'password',
    ];

    /**
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return $this->primaryKey;
    }

    /**
     * @return mixed
     */
    public function getAuthIdentifier() {
        $field_name = $this->getAuthIdentifierName();
        return $this->attributes[$field_name];
    }

    /**
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    /**
     * @return string
     */
    public function getRememberTokenName()
    {
        return $this->rememberToken;
    }

}
