<?php

namespace App\Services\Apis\Resources\Contracts;

use App;
use BDRequest;

abstract class BaseApiResource {

    public static function getBDInstance() {
        return new BDRequest(App::make('bd-client'));
    }

    abstract public static function all();

    abstract public static function find($id);

}
