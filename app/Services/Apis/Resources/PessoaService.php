<?php
namespace App\Services\Apis\Resources;

use App;
use BDParams;

use App\Models\Auth\Usuario;

use App\Services\Apis\Resources\Contracts\BaseApiResource;

class PessoaService extends BaseApiResource {

    public static function all() {
        return null;
    }

    public static function find($id) {
        $dbInstance = self::getBDInstance();

        $apiPerson = collect($dbInstance->request('/pessoa', BDParams::SEARCH, [
            "id" => $id
        ]));

        if(!isset($apiPerson)) return null;

        $user = new Usuario();
        $user->fill((array) $apiPerson->first());

        return $user;

    }

    public static function findByIdentifier($identifier) {
        $dbInstance = self::getBDInstance();

        $apiPerson = collect($dbInstance->request('/pessoa', BDParams::QUERY, [
            "identifier" => $identifier
        ]));

        if(!isset($apiPerson)) return null;

        $persons = (array) $apiPerson->first();

        if(!isset($persons[0])) return null; # medida necessária até correção da API do banco

        $user = new Usuario();
        $user->fill((array) $persons[0]);

        return $user;

    }

    public static function checkCredentials(array $credentials) {
        $path = $credentials["identificacao"] . '/' . $credentials["password"];

        return self::getBDInstance()->request($path, BDParams::AUTH);

    }

}
