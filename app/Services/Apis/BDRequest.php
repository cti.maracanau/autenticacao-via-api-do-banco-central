<?php
namespace App\Services\Apis;

use GuzzleHttp\Client;

use Exception;
use BDParams;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;

class BDRequest {

    private $client;
    private $password;
    private $headers;

    public function __construct(Client $client) {
        $this->client = $client;
        $this->password = env('BD_API_PASSWORD');
        $this->headers = [];

    }

    public function request($url, $type, array $params = []) {
        try{
            $relative_path = $this->buildRelativePath($url, $type, $params);

            $request = $this->client->get($relative_path, [
                'headers' => $this->headers,
            ]);

            if(!isset($request)) return null;

            $response = $request->getBody()->getContents();
            $status = $request->getStatusCode();

            if($response && $status === 200 && strpos($response, 'resultados')) {
                return json_decode($response)->resultados;
            }
            if($response && $status === 200 && $type == BDParams::AUTH) {

                return $response == 'true' ? true : false;
            }

            return null;

        } catch(Exception $e) {
            return null;

        }

    }

    private function buildRelativePath($url, $type, array $params = []) {
        $relative_path = $type . '/';

        switch($type) {
            case BDParams::QUERY:
            case BDParams::SEARCH:
                $relative_path .= $this->password . $url;
                break;

            case BDParams::AUTH:
                $relative_path .= $url;
                break;

            default:
                $relative_path .= $url;
        }

        foreach($params as $key => $value) {
            $relative_path .= "/" . $value;
        }

        return $relative_path;
    }

}
